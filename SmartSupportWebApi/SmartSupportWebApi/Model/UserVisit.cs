﻿using MySqlDbController;

namespace SmartSupportWebApi.Model
{
    [Table("user_visit")]
    public class UserVisit : DbObject<int>
    {
        [Column("accused_id")]
        [ColumnSettings(ColumnDataType.INT, 11, -1, true)]
        public int AccusedId { get; set; }

        [Column("sitting_id")]
        [ColumnSettings(ColumnDataType.INT, 11, -1, true)]
        public int SittingId { get; set; }

        [Column("user_name")]
        [ColumnSettings(ColumnDataType.VARCHAR, 255, null, true)]
        public string UserName { get; set; }

        public UserVisit() : base() { }

        public UserVisit(int accusedId, int sittingId, string userName) : base(-1)
        {
            AccusedId = accusedId;
            SittingId = sittingId;
            UserName = userName;
        }
    }
}
