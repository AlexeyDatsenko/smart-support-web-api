﻿using MySqlDbController;
using Newtonsoft.Json;
using System;

namespace SmartSupportWebApi.Model
{
    [Table("sitting")]
    public class Sitting : DbObject<int>
    {
        [Column("accused_id")]
        [ColumnSettings(ColumnDataType.INT, 11, -1, true)]
        [JsonProperty(PropertyName = "accused_id")]
        public int AccusedId { get; set; }

        [Column("visitors_count")]
        [ColumnSettings(ColumnDataType.INT, 11, 0, true)]
        [JsonProperty(PropertyName = "visitors_count")]
        public int VisitorsCount { get; set; }

        [Column("date_and_time")]
        [ColumnSettings(ColumnDataType.DATETIME)]
        [JsonProperty(PropertyName = "date_and_time")]
        public DateTime DateAndTime { get; set; }

        [Column("address")]
        [ColumnSettings(ColumnDataType.VARCHAR, 255)]
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }

        [Column("chat_link")]
        [ColumnSettings(ColumnDataType.TEXT)]
        [JsonProperty(PropertyName = "chat_link")]
        public string ChatLink { get; set; }

        public Sitting() : base() { }

        public Sitting(int accusedId, int visitorsCount, DateTime dateAndTime, string address, string chatLink) : base(-1)
        {
            AccusedId = accusedId;
            VisitorsCount = visitorsCount;
            DateAndTime = dateAndTime;
            Address = address;
            ChatLink = chatLink;
        }
    }
}
