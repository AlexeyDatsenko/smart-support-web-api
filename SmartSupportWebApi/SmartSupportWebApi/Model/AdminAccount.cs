﻿using MySqlDbController;
using Newtonsoft.Json;

namespace SmartSupportWebApi.Model
{
    [Table("admin_account")]
    public class AdminAccount : DbObject<int>
    {
        [Column("user_name")]
        [ColumnSettings(ColumnDataType.VARCHAR, 255, null, true)]
        [JsonProperty(PropertyName = "user_name")]
        public string UserName { get; set; }

        [Column("password_hash")]
        [ColumnSettings(ColumnDataType.VARCHAR, 255, null, true)]
        [JsonProperty(PropertyName = "password_hash")]
        public string PasswordHash { get; set; }

        public AdminAccount() : base() { }

        public AdminAccount(string userName, string passwordHash) : base(-1)
        {
            UserName = userName;
            PasswordHash = passwordHash;
        }
    }
}
