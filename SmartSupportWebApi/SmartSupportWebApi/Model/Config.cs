﻿using Newtonsoft.Json;
using System.IO;
using System.Text;

namespace SmartSupportWebApi.Model
{
    public class Config
    {
        private static string PATH_OF_CONFIG = Directory.GetCurrentDirectory() + @"\config";

        private static Config ConfigInstance { get; set; }

        public string Host { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DataBaseName { get; set; }

        public Config() { }

        private Config(string host, string userName, string password, string dataBaseName)
        {
            Host = host;
            UserName = userName;
            Password = password;
            DataBaseName = dataBaseName;
        }

        public static Config GetInstance()
        {
            if (ConfigInstance == null)
                ReadConfig();

            return ConfigInstance;
        }

        private static void ReadConfig()
        {
            if (!File.Exists(PATH_OF_CONFIG))
            {
                var config = new Config("172.17.0.2", "server", "1905", "smart_support");

                string strTMP = JsonConvert.SerializeObject(config);

                using (var fileStream = File.Create(PATH_OF_CONFIG))
                {
                    var infoByteArray = new UTF8Encoding(true).GetBytes(strTMP);
                    fileStream.Write(infoByteArray, 0, infoByteArray.Length);
                }
            }

            using (var streamReader = File.OpenText(PATH_OF_CONFIG))
            {
                var content = streamReader.ReadLine();
                ConfigInstance = JsonConvert.DeserializeObject<Config>(content);
            }
        }
    }
}
