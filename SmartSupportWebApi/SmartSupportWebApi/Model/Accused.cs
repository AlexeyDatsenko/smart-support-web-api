﻿using MySqlDbController;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace SmartSupportWebApi.Model
{
    [Table("accused")]
    public class Accused : DbObject<int>
    {
        [Column("photo")]
        [ColumnSettings(ColumnDataType.LONGBLOB)]
        [JsonProperty(PropertyName = "photo")]
        public byte[] Photo { get; set; }

        [Column("name")]
        [ColumnSettings(ColumnDataType.VARCHAR, 255, null, true)]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [Column("about")]
        [ColumnSettings(ColumnDataType.TEXT)]
        [JsonProperty(PropertyName = "about")]
        public string About { get; set; }

        [Column("articles")]
        [ColumnSettings(ColumnDataType.TEXT)]
        [JsonProperty(PropertyName = "articles")]
        public string Articles { get; set; }

        [Column("essence_of_the_charge")]
        [ColumnSettings(ColumnDataType.TEXT)]
        [JsonProperty(PropertyName = "essence_of_the_charge")]
        public string EssenceOfTheCharge { get; set; }

        [Column("address_for_support")]
        [ColumnSettings(ColumnDataType.VARCHAR, 255)]
        [JsonProperty(PropertyName = "address_for_support")]
        public string AddressForSupport { get; set; }

        [Column("address_for_money_support")]
        [ColumnSettings(ColumnDataType.VARCHAR, 255)]
        [JsonProperty(PropertyName = "address_for_money_support")]
        public string AddressForMoneySupport { get; set; }

        [JsonProperty(PropertyName = "soon_sittings")]
        public List<Sitting> SoonSittings { get; set; }

        public Accused() : base() { }

        public Accused(byte[] photo, string name, string about, string articles, string essenceOfTheCharge, string addressForSupport, string addressForMoneySupport, List<Sitting> soonSittings) : base(-1)
        {
            Photo = photo;
            Name = name;
            About = about;
            Articles = articles;
            EssenceOfTheCharge = essenceOfTheCharge;
            AddressForSupport = addressForSupport;
            AddressForMoneySupport = addressForMoneySupport;
            SoonSittings = soonSittings;
        }
    }
}
