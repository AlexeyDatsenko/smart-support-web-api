﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using MySqlDbController;
using SmartSupportWebApi.Model;

namespace SmartSupportWebApi
{
    public class Startup
    {
        readonly string myAllowSpecimicOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            const string signingSecurityKey = DataProvider.PUBLIC_KEY;
            var signingKey = new SigningSymmetricKey(signingSecurityKey);
            services.AddSingleton<IJwtSigningEncodingKey>(signingKey);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            var jwtSchemeName = "JwtBearer";
            var signingDecodingKey = (IJwtSigningDecodingKey)signingKey;

            services.AddCors(options =>
            {
                options.AddPolicy(myAllowSpecimicOrigins,
                builder =>
                {
                    builder.WithOrigins("http://185.146.156.106:5002").AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin(); 
                });
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = jwtSchemeName;
                options.DefaultChallengeScheme = jwtSchemeName;
            })
                .AddJwtBearer(jwtSchemeName, jwtBearerOptions =>
                {
                    jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = signingDecodingKey.GetKey(),
                        ValidateIssuer = true,
                        ValidIssuer = "SmartSupportWebApi",
                        ValidateAudience = true,
                        ValidAudience = "SmartSupportClient",
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.FromSeconds(5)
                    };
                });

            CreateDataBase();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(myAllowSpecimicOrigins);
            app.UseAuthentication();
            app.UseMvc();
        }

        private void CreateDataBase()
        {
            var config = Config.GetInstance();
            var accusedController = new DbController<Accused, int>(config.Host, config.UserName, config.Password, config.DataBaseName);
            var sittingController = new DbController<Sitting, int>(config.Host, config.UserName, config.Password, config.DataBaseName);
            var userVisitController = new DbController<UserVisit, int>(config.Host, config.UserName, config.Password, config.DataBaseName);
            var adminAccountController = new DbController<AdminAccount, int>(config.Host, config.UserName, config.Password, config.DataBaseName);

            accusedController.CreateDbIfNotExists();
            accusedController.CreateTableIfNotExists();
            sittingController.CreateTableIfNotExists();
            userVisitController.CreateTableIfNotExists();
            adminAccountController.CreateTableIfNotExists();
        }
    }
}
