﻿using Microsoft.IdentityModel.Tokens;

namespace SmartSupportWebApi
{
    public interface IJwtSigningEncodingKey
    {
        string SigningAlgorithm { get; }
        SecurityKey GetKey();
    }
}
