﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MySqlDbController;
using SmartSupportWebApi.Model;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace SmartSupportWebApi.Controllers
{
    [Route("api/authorization")]
    [ApiController]
    public class Athorization : ControllerBase
    {
        [HttpPost]
        [AllowAnonymous]
        public ActionResult<string> GetToken([FromBody] AdminAccount adminAccount, [FromServices] IJwtSigningEncodingKey signingEncodingKey)
        {
            var config = Config.GetInstance();
            var controller = new DbController<AdminAccount, int>(config.Host, config.UserName, config.Password, config.DataBaseName);
            var existsCheck = controller.Select(1, 30, null, "user_name LIKE '" + adminAccount.UserName + "' AND password_hash LIKE '" + adminAccount.PasswordHash + "'");

            if (existsCheck == null)
                return BadRequest("Invalid login or password");

            var claims = new Claim[] { new Claim(ClaimTypes.NameIdentifier, adminAccount.UserName) };

            var token = new JwtSecurityToken(
                issuer: "SmartSupportWebApi",
                audience: "SmartSupportClient",
                claims: claims,
                expires: DateTime.Now.AddHours(24),
                signingCredentials: new SigningCredentials(
                    signingEncodingKey.GetKey(),
                    signingEncodingKey.SigningAlgorithm));

            var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);

            return Ok(jwtToken);
        }
    }
}
