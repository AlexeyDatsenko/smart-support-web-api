﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SmartSupportWebApi.Model;

namespace SmartSupportWebApi.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        [HttpGet]
        [Route("get_all_accuseds")]
        public async Task<ActionResult<List<Accused>>> GetAllAccuseds()
        {
            var task = Task.Run(() => new DataProvider().GetAllAccused());
            await task;

            return Ok(task);
        }

        [HttpGet]
        [Route("get_all_sittings")]
        public async Task<ActionResult<List<Sitting>>> GetAllSittings()
        {
            var task = Task.Run(() => new DataProvider().GetAllSittings());
            await task;

            return Ok(task);
        }

        [HttpPut]
        [Route("put_user_visit")]
        public async Task<ActionResult> PutUserVisit([FromQuery(Name = "user_name")] string userName, [FromQuery(Name = "sitting_id")] int sittingId, [FromQuery(Name = "accused_id")] int accusedId)
        {
            var task = Task.Run(() => new DataProvider().PutUserVisit(userName, sittingId, accusedId));
            await task;

            if (task.Result)
                return Ok();
            else
                return BadRequest();
        }

        [HttpDelete]
        [Route("delete_user_visit")]
        public async Task<ActionResult> DeleteUserVisit([FromQuery(Name = "user_name")] string userName, [FromQuery(Name = "sitting_id")] int sittingId, [FromQuery(Name = "accused_id")] int accusedId)
        {
            var task = Task.Run(() => new DataProvider().DeleteUserVisit(userName, sittingId, accusedId));
            await task;

            if (task.Result)
                return Ok();
            else
                return BadRequest();
        }
    }
}
