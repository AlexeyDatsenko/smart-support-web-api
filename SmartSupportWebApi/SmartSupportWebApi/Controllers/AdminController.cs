﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartSupportWebApi.Model;
using System.Threading.Tasks;

namespace SmartSupportWebApi.Controllers
{
    [Route("api/admin")]
    [ApiController]
    [Authorize]
    public class AdminController : ControllerBase
    {
        [HttpPut]
        [Route("put_accused")]
        public async Task<ActionResult> PutAccused([FromBody] Accused accused)
        {
            var task = Task.Run(() => new DataController().PutAccused(accused));
            await task;

            if (task.Result)
                return Ok();
            else
                return BadRequest();
        }

        [HttpPatch]
        [Route("update_accused")]
        public async Task<ActionResult> UpdateAccused([FromBody] Accused accused)
        {
            var task = Task.Run(() => new DataController().UpdateAccused(accused));
            await task;

            if (task.Result)
                return Ok();
            else
                return BadRequest();
        }

        [HttpDelete]
        [Route("delete_accused")]
        public async Task<ActionResult> DeleteAccused([FromQuery(Name = "id")] int id)
        {
            var task = Task.Run(() => new DataController().DeleteAccused(id));
            await task;

            if (task.Result)
                return Ok();
            else
                return BadRequest();
        }

        [HttpPut]
        [Route("put_sitting")]
        public async Task<ActionResult> PutSitting([FromBody] Sitting sitting)
        {
            var task = Task.Run(() => new DataController().PutSitting(sitting));
            await task;

            if (task.Result)
                return Ok();
            else
                return BadRequest();
        }

        [HttpPatch]
        [Route("update_sitting")]
        public async Task<ActionResult> UpdateSitting([FromBody] Sitting sitting)
        {
            var task = Task.Run(() => new DataController().UpdateSitting(sitting));
            await task;

            if (task.Result)
                return Ok();
            else
                return BadRequest();
        }

        [HttpDelete]
        [Route("delete_sitting")]
        public async Task<ActionResult> DeleteSitting([FromQuery(Name = "id")] int id)
        {
            var task = Task.Run(() => new DataController().DeleteSitting(id));
            await task;

            if (task.Result)
                return Ok();
            else
                return BadRequest();
        }
    }
}
