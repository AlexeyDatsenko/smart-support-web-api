﻿using MySqlDbController;
using SmartSupportWebApi.Model;
using System.Collections.Generic;

namespace SmartSupportWebApi
{
    public class DataProvider
    {
        public const string PUBLIC_KEY = "UoxGkbE0azuTL1T3tiHmbcHeiuDXpPdcfEvKwwBwH0Airao6";

        public List<Accused> GetAllAccused()
        {
            var config = Config.GetInstance();
            var accusedController = new DbController<Accused, int>(config.Host, config.UserName, config.Password, config.DataBaseName);
            var sittingController = new DbController<Sitting, int>(config.Host, config.UserName, config.Password, config.DataBaseName);

            var accusedList = accusedController.Select();
            var sittingsList = sittingController.Select(10000, 30, null, "date_and_time > NOW()");

            foreach (var accused in accusedList)
                accused.SoonSittings = sittingsList.FindAll(x => x.AccusedId == accused.Id);

            return accusedList;
        }

        public List<Sitting> GetAllSittings()
        {
            var config = Config.GetInstance();
            var sittingController = new DbController<Sitting, int>(config.Host, config.UserName, config.Password, config.DataBaseName);

            var sittingsList = sittingController.Select(10000, 30, null, "date_and_time > NOW()", null, "visitors_count");

            return sittingsList;
        }

        public bool PutUserVisit(string userName, int sittingId, int accusedId)
        {
            var config = Config.GetInstance();
            var userVisitController = new DbController<UserVisit, int>(config.Host, config.UserName, config.Password, config.DataBaseName);
            var sittingController = new DbController<Sitting, int>(config.Host, config.UserName, config.Password, config.DataBaseName);

            var existsCheck = userVisitController.Select(10000, 60, null, "sitting_id = " + sittingId.ToString() + " AND user_name LIKE '" + userName + "'");

            if (existsCheck.Count != 0)
                return false;
            else
            {
                userVisitController.Insert(new UserVisit(accusedId, sittingId, userName));
                var targetSitting = sittingController.SelectById(sittingId);
                targetSitting.VisitorsCount++;
                sittingController.Update(targetSitting);

                return true;
            }
        }

        public bool DeleteUserVisit(string userName, int sittingId, int accusedId)
        {
            var config = Config.GetInstance();
            var userVisitController = new DbController<UserVisit, int>(config.Host, config.UserName, config.Password, config.DataBaseName);
            var sittingController = new DbController<Sitting, int>(config.Host, config.UserName, config.Password, config.DataBaseName);

            var existsCheck = userVisitController.Select(10000, 60, null, "sitting_id = " + sittingId.ToString() + " AND user_name LIKE '" + userName + "'");

            if (existsCheck.Count == 0)
                return false;
            else
            {
                userVisitController.Delete(existsCheck[0].Id);
                var targetSitting = sittingController.SelectById(sittingId);
                targetSitting.VisitorsCount--;
                sittingController.Update(targetSitting);

                return true;
            }
        }
    }
}
