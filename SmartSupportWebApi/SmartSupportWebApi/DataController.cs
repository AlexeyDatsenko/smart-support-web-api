﻿using MySqlDbController;
using SmartSupportWebApi.Model;

namespace SmartSupportWebApi
{
    public class DataController
    {
        public bool PutAccused(Accused accused)
        {
            var config = Config.GetInstance();
            var controller = new DbController<Accused, int>(config.Host, config.UserName, config.Password, config.DataBaseName);

            try
            {
                controller.Insert(accused);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateAccused(Accused accused)
        {
            var config = Config.GetInstance();
            var controller = new DbController<Accused, int>(config.Host, config.UserName, config.Password, config.DataBaseName);

            try
            {
                controller.Update(accused);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAccused(int id)
        {
            var config = Config.GetInstance();
            var controller = new DbController<Accused, int>(config.Host, config.UserName, config.Password, config.DataBaseName);

            try
            {
                controller.Delete(id);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool PutSitting(Sitting sitting)
        {
            var config = Config.GetInstance();
            var controller = new DbController<Sitting, int>(config.Host, config.UserName, config.Password, config.DataBaseName);

            try
            {
                controller.Insert(sitting);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateSitting(Sitting sitting)
        {
            var config = Config.GetInstance();
            var controller = new DbController<Sitting, int>(config.Host, config.UserName, config.Password, config.DataBaseName);

            try
            {
                controller.Update(sitting);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteSitting(int id)
        {
            var config = Config.GetInstance();
            var controller = new DbController<Sitting, int>(config.Host, config.UserName, config.Password, config.DataBaseName);

            try
            {
                controller.Delete(id);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
