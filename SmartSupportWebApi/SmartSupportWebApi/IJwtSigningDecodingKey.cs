﻿using Microsoft.IdentityModel.Tokens;

namespace SmartSupportWebApi
{
    public interface IJwtSigningDecodingKey
    {
        SecurityKey GetKey();
    }
}
